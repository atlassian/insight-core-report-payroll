package com.riadalabs.jira.plugins.insight.reports.payroll;

import java.time.format.DateTimeFormatter;

public final class Constants {

    public static final DateTimeFormatter IQL_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00");

}
