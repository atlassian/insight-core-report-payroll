package com.riadalabs.jira.plugins.insight.reports.payroll;

import java.util.Map;

public class Expenditure {

    private String name;
    private Map<ExpenditureType, Double> typeValueMap;

    public Expenditure(String name, Map<ExpenditureType, Double> typeValueMap) {
        this.name = name;
        this.typeValueMap = typeValueMap;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<ExpenditureType, Double> getTypeValueMap() {
        return typeValueMap;
    }

    public void setTypeValueMap(Map<ExpenditureType, Double> typeValueMap) {
        this.typeValueMap = typeValueMap;
    }
}
