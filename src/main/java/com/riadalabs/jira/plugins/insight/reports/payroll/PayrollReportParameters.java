package com.riadalabs.jira.plugins.insight.reports.payroll;

import com.google.common.collect.Lists;
import io.riada.jira.plugins.insight.widget.api.WidgetParameters;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * This is what is sent from the frontend as expected inputs
 */
public class PayrollReportParameters implements WidgetParameters {

    private Value schema;
    private Value objectType;

    private Value numAttribute;
    private Value dateAttributeStartDate;
    private Value dateAttributeEndDate;

    private String period;
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    private String iql;

    public PayrollReportParameters() {
    }

    public Value getSchema() {
        return schema;
    }

    public void setSchema(Value schema) {
        this.schema = schema;
    }

    public Value getObjectType() {
        return objectType;
    }

    public void setObjectType(Value objectType) {
        this.objectType = objectType;
    }

    public Value getNumAttribute() {
        return numAttribute;
    }

    public void setNumAttribute(Value numAttribute) {
        this.numAttribute = numAttribute;
    }

    public Value getDateAttributeStartDate() {
        return dateAttributeStartDate;
    }

    public void setDateAttributeStartDate(Value dateAttributeStartDate) {
        this.dateAttributeStartDate = dateAttributeStartDate;
    }

    public Value getDateAttributeEndDate() {
        return dateAttributeEndDate;
    }

    public void setDateAttributeEndDate(Value dateAttributeEndDate) {
        this.dateAttributeEndDate = dateAttributeEndDate;
    }

    public String getPeriod() {
        return period;
    }

    public Period period() {

        return Period.from(getPeriod());
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getIql() {
        return iql;
    }

    public void setIql(String iql) {
        this.iql = iql;
    }

    public LocalDate determineStartDate(Boolean doesWeekBeginOnMonday) {
        return this.period() == Period.CUSTOM ? getStartDate().toLocalDate()
                : this.period().from(LocalDate.now(), doesWeekBeginOnMonday);
    }

    public LocalDate determineEndDate(Boolean doesWeekBeginOnMonday) {
        final LocalDate today = LocalDate.now();

        return this.period() == Period.CUSTOM ? Period.findEarliest(getEndDate().toLocalDate(), today)
                : this.period().to(today, doesWeekBeginOnMonday);
    }

    public List<Value> getDateAttributes() {
        return Lists.newArrayList(getDateAttributeStartDate(), getDateAttributeEndDate());
    }

    public List<Value> getNumericAttributes() {
        return Lists.newArrayList(getNumAttribute());
    }
}
