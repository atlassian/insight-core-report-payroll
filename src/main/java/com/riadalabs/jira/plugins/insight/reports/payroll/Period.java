package com.riadalabs.jira.plugins.insight.reports.payroll;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;

import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;

public enum Period {

    CURRENT_WEEK {
        @Override
        public LocalDate from(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date.with(Period.firstDayOfWeek(doesWeekBeginOnMonday), 1L);
        }

        @Override
        public LocalDate to(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return Period.findEarliest(date, date.with(firstDayOfWeek(doesWeekBeginOnMonday), 7L));
        }
    },

    CURRENT_MONTH {
        @Override
        public LocalDate from(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date.with(firstDayOfMonth());
        }

        @Override
        public LocalDate to(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return findEarliest(date, date.with(lastDayOfMonth()));
        }
    },

    CURRENT_YEAR {
        @Override
        public LocalDate from(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date.with(firstDayOfYear());
        }

        @Override
        public LocalDate to(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return Period.findEarliest(date, date.with(lastDayOfYear()));
        }
    },

    LAST_WEEK {
        @Override
        public LocalDate from(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date.with(firstDayOfWeek(doesWeekBeginOnMonday), 1L)
                    .minusWeeks(1);
        }

        @Override
        public LocalDate to(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date.with(firstDayOfWeek(doesWeekBeginOnMonday), 7L)
                    .minusWeeks(1);
        }
    },

    LAST_MONTH {
        @Override
        public LocalDate from(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date.with(firstDayOfMonth())
                    .minusMonths(1);
        }

        @Override
        public LocalDate to(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date.with(lastDayOfMonth())
                    .minusMonths(1);
        }
    },

    LAST_YEAR {
        @Override
        public LocalDate from(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date.minusYears(1)
                    .with(firstDayOfYear());
        }

        @Override
        public LocalDate to(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date.minusYears(1)
                    .with(lastDayOfYear());
        }
    },

    CUSTOM {
        @Override
        public LocalDate from(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date;
        }

        @Override
        public LocalDate to(LocalDate date, Boolean doesWeekBeginOnMonday) {
            return date;
        }
    };

    public abstract LocalDate from(LocalDate date, Boolean doesWeekBeginOnMonday);

    public abstract LocalDate to(LocalDate date, Boolean doesWeekBeginOnMonday);

    public static Period from(final String name) {
        if (StringUtils.isEmpty(name)) {
            return defaultPeriod();
        }

        try {
            return valueOf(name.toUpperCase());
        } catch (IllegalArgumentException e) {
            return defaultPeriod();
        }
    }

    public static LocalDate findEarliest(LocalDate date1, LocalDate date2) {
        return date1.isBefore(date2) ? date1 : date2;
    }

    private static TemporalField firstDayOfWeek(Boolean doesWeekBeginOnMonday) {
        return BooleanUtils.isTrue(doesWeekBeginOnMonday) ? WeekFields.ISO.dayOfWeek()
                : WeekFields.SUNDAY_START.dayOfWeek();
    }

    public static Period defaultPeriod() {
        return CURRENT_WEEK;
    }
}
