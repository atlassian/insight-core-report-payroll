package com.riadalabs.jira.plugins.insight.reports.payroll;

import io.riada.core.annotation.Generated;

import java.util.Objects;

/**
 * Attributes from FE are wrapped in Value object
 */
public final class Value {

    private Integer value;

    private Value() {
    }

    public Value(final Integer value) {
        setValue(value);
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Generated(by = "IDE")
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Value value1 = (Value) o;
        return value.equals(value1.value);
    }

    @Generated(by = "IDE")
    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

}
