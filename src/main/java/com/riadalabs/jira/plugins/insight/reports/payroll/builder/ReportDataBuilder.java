package com.riadalabs.jira.plugins.insight.reports.payroll.builder;

import com.google.common.collect.Maps;
import com.riadalabs.jira.plugins.insight.reports.payroll.Expenditure;
import com.riadalabs.jira.plugins.insight.reports.payroll.ExpenditureType;
import com.riadalabs.jira.plugins.insight.reports.payroll.PayrollReportData;
import com.riadalabs.jira.plugins.insight.services.model.ObjectAttributeBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectAttributeValueBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectBean;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class ReportDataBuilder {

    private final AtomicBoolean hasData = new AtomicBoolean(false);

    private Map<LocalDate, Map<String, Map<ExpenditureType, Double>>> daysWithDataMap;
    private boolean useIso8601FirstDayOfWeek;
    private Map<Integer, String> numericAttributeIdToNameMap;
    private LinkedHashMap<Integer, ObjectAttributeBean> dateAttributeIdToNameMap;

    public ReportDataBuilder(final LocalDate startDate, final LocalDate endDate, boolean useIso8601FirstDayOfWeek,
                             final Map<Integer, String> numericAttributeNames,
                             final LinkedHashMap<Integer, ObjectAttributeBean> dateAttributesMap) {

        buildDaysWithEmptyMap(startDate, endDate);
        this.useIso8601FirstDayOfWeek = useIso8601FirstDayOfWeek;
        this.numericAttributeIdToNameMap = numericAttributeNames;
        this.dateAttributeIdToNameMap = dateAttributesMap;
    }

    private void buildDaysWithEmptyMap(final LocalDate startDate, final LocalDate endDate) {
        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);

        this.daysWithDataMap = LongStream.rangeClosed(0, numOfDaysBetween)
                .boxed()
                .collect(Collectors.toMap(
                        day -> startDate.plusDays(day),
                        day -> new LinkedHashMap<>(),
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }

    public ReportDataBuilder fillData(final List<ObjectBean> objectBeans){
        objectBeans.forEach(this::fillDataPerDay);

        return this;
    }

    private void fillDataPerDay(final ObjectBean objectBean) {
        final List<ObjectAttributeBean> validDateAttributes = filterValidDateAttributes(objectBean);
        final List<ObjectAttributeBean> validNumericAttributes = filterValidNumericAttributes(objectBean);

        if (isReportable(validDateAttributes, validNumericAttributes)) {
            final LocalDate startDate = getStartDate(validDateAttributes);
            final LocalDate endDate = getEndDate(validDateAttributes);

            validNumericAttributes.forEach(numericAttribute -> {
                final String key = this.numericAttributeIdToNameMap.get(numericAttribute.getObjectTypeAttributeId());

                final Number numericValue = (Number) getANonNullValue(numericAttribute).getValue();
                final Double numericValueAsDouble = numericValue.doubleValue();

                if (isWithinDateRange(startDate)) {
                    expenditureIn(startDate, key, numericValueAsDouble);
                }

                if (isWithinDateRange(endDate)) {
                    expenditureOut(endDate, key, numericValueAsDouble);
                }

                hasData.set(true);
            });
        }
    }

    private List<ObjectAttributeBean> filterValidDateAttributes(final ObjectBean objectBean) {
        objectBean.getObjectAttributeBeans()
                .stream()
                .filter(attribute -> this.dateAttributeIdToNameMap.containsKey(attribute.getObjectTypeAttributeId()))
                .forEach(attribute -> this.dateAttributeIdToNameMap.put(attribute.getObjectTypeAttributeId(), attribute));

        return this.dateAttributeIdToNameMap.entrySet().stream()
                .filter(entry -> Objects.nonNull(entry.getValue()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    private List<ObjectAttributeBean> filterValidNumericAttributes(final ObjectBean objectBean) {
        return objectBean.getObjectAttributeBeans()
                .stream()
                .filter(attribute -> this.numericAttributeIdToNameMap.containsKey(attribute.getObjectTypeAttributeId())
                        && hasNonNullValue(attribute))
                .collect(Collectors.toList());

    }

    private boolean isReportable(final List<ObjectAttributeBean> validDateAttributes,
            final List<ObjectAttributeBean> validNumericAttributes) {

        return containsANonNullValue(validDateAttributes) && !validNumericAttributes.isEmpty();
    }

    private boolean containsANonNullValue(final List<ObjectAttributeBean> attributes) {
        return attributes.stream()
                .anyMatch(Objects::nonNull);
    }

    private LocalDate getStartDate(final List<ObjectAttributeBean> dateAttributes) {
        return safeGetDate(dateAttributes.get(0));
    }

    private LocalDate getEndDate(final List<ObjectAttributeBean> dateAttributes) {
        return dateAttributes.size() > 1 ? safeGetDate(dateAttributes.get(1))
                : null;
    }

    private LocalDate safeGetDate(ObjectAttributeBean attributeBean) {
        return hasNonNullValue(attributeBean) ? getANonNullValue(attributeBean).getDateValue()
                .toInstant()
                .atZone(ZoneId.of("UTC"))
                .toLocalDate() : null;
    }

    private ObjectAttributeValueBean getANonNullValue(ObjectAttributeBean attributeBean) {
        return attributeBean.getObjectAttributeValueBeans()
                .stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Could not find a value for: " + attributeBean.toString()));
    }

    private boolean hasNonNullValue(ObjectAttributeBean attribute) {
        if (attribute.getObjectAttributeValueBeans().isEmpty()) {

            return false;
        }

        Optional<? extends ObjectAttributeValueBean> valueBean = attribute.getObjectAttributeValueBeans()
                .stream()
                .findFirst();

        return valueBean.isPresent() && valueBean.get().getValue() != null;
    }

    private boolean isWithinDateRange(LocalDate date){
        return Objects.nonNull(date) && this.daysWithDataMap.containsKey(date);
    }

    private void expenditureIn(final LocalDate startDate, final String attributeKey, final Double amount) {
        final Map<String, Map<ExpenditureType, Double>> attributeToExpenditure = this.daysWithDataMap.get(startDate);

        if (attributeToExpenditure.isEmpty() || !attributeToExpenditure.containsKey(attributeKey)) {
            setEmptyExpenditureMapForDate(startDate, attributeKey);
        }

        attributeToExpenditure
                .get(attributeKey)
                .compute(ExpenditureType.IN,
                        (key, map) -> map + amount);

    }

    private void expenditureOut(final LocalDate endDate, final String attributeKey, final Double amount) {
        final Map<String, Map<ExpenditureType, Double>> attributeToExpenditure = this.daysWithDataMap.get(endDate);

        if (attributeToExpenditure.isEmpty() || !attributeToExpenditure.containsKey(attributeKey)) {
            setEmptyExpenditureMapForDate(endDate, attributeKey);
        }

        attributeToExpenditure
                .get(attributeKey)
                .compute(ExpenditureType.OUT,
                        (key, map) -> map + amount);

    }

    private void setEmptyExpenditureMapForDate(final LocalDate date, final String attributeKey) {
        final Map<ExpenditureType, Double> expenditureTypeMap = Maps.newLinkedHashMap();
        expenditureTypeMap.put(ExpenditureType.IN, 0.0);
        expenditureTypeMap.put(ExpenditureType.OUT, 0.0);

        this.daysWithDataMap.get(date)
                .put(attributeKey, expenditureTypeMap);
    }

    public PayrollReportData build() {
        if (hasData.get()) {
            return new PayrollReportData(toExpenditureMap(this.daysWithDataMap), true, this.useIso8601FirstDayOfWeek);
        }

        return PayrollReportData.empty();
    }

    private Map<LocalDate, List<Expenditure>> toExpenditureMap(Map<LocalDate, Map<String, Map<ExpenditureType, Double>>> daysWithDataMap) {
        return daysWithDataMap.entrySet().stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> toExpenditureList(entry.getValue()),
                        (o1, o2) -> o1,
                        LinkedHashMap::new
                ));
    }

    private List<Expenditure> toExpenditureList(Map<String, Map<ExpenditureType, Double>> attributeToExpenditureMap) {
        return attributeToExpenditureMap.entrySet().stream()
                .map(entry -> new Expenditure(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

    }

}
