package com.riadalabs.jira.plugins.insight.reports.payroll.validator;

import com.google.common.collect.Sets;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectSchemaFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeAttributeFacade;
import com.riadalabs.jira.plugins.insight.common.exception.InsightException;
import com.riadalabs.jira.plugins.insight.reports.payroll.PayrollReportParameters;
import com.riadalabs.jira.plugins.insight.reports.payroll.Period;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeAttributeBean;
import io.riada.core.service.model.ServiceError;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class PayrollReportValidator {

    private final static Set<ObjectTypeAttributeBean.DefaultType> validNumericTypes = Sets.newHashSet(
            ObjectTypeAttributeBean.DefaultType.INTEGER,
            ObjectTypeAttributeBean.DefaultType.DOUBLE);

    private final static Set<ObjectTypeAttributeBean.DefaultType> validDateTypes = Sets.newHashSet(
            ObjectTypeAttributeBean.DefaultType.DATE,
            ObjectTypeAttributeBean.DefaultType.DATE_TIME);

    private final static String dateParameterName = "dateAttributes";
    private final static String numberParameterName = "numAttribute";

    public static Set<ServiceError> validate(final PayrollReportParameters reportParameters, final ObjectSchemaFacade objectSchemaFacade,
            final ObjectTypeAttributeFacade objectTypeAttributeFacade) throws InsightException {

        final Set<ServiceError> errors = Sets.newHashSet();

        errors.addAll(validateSchema(reportParameters, objectSchemaFacade));
        errors.addAll(validateObjectType(reportParameters));

        //Unable to find a schema or object type to evaluate
        if (!errors.isEmpty()) {
            return errors;
        }

        List<ObjectTypeAttributeBean> attributes = objectTypeAttributeFacade.findObjectTypeAttributeBeans(reportParameters.getObjectType()
                .getValue());

        errors.addAll(validateObjectTypeAttributes(attributes));

        // Unable to find any attributes to evaluate
        if (!errors.isEmpty()) {
           return errors;
        }

        final Map<Integer, ObjectTypeAttributeBean> objectTypeAttributeBeanMap = attributes.stream()
                .collect(Collectors.toMap(
                        ObjectTypeAttributeBean::getId,
                        objectTypeAttributeBean -> objectTypeAttributeBean,
                        (v1, v2) -> v1));

        errors.addAll(reportParameters.getDateAttributes().stream()
                .flatMap(dateAttribute ->
                        validateTypes(dateParameterName, dateAttribute.getValue(), objectTypeAttributeBeanMap,
                                validDateTypes).stream())
                .collect(Collectors.toSet()));

        errors.addAll(validateTypes(numberParameterName, reportParameters.getNumAttribute().getValue(),
                objectTypeAttributeBeanMap, validNumericTypes));

        errors.addAll(
                validateDates(reportParameters.period(),
                        reportParameters.determineStartDate(false).atStartOfDay(),
                        reportParameters.determineEndDate(false).atStartOfDay()));

        return errors;
    }

    private static Set<ServiceError> validateSchema(final PayrollReportParameters reportParameters,
            final ObjectSchemaFacade objectSchemaFacade) throws InsightException {

        final Set<ServiceError> schemaErrors = Sets.newHashSet();

        if (Objects.isNull(reportParameters.getSchema()) || Objects.isNull(objectSchemaFacade.loadObjectSchemaBean(
                reportParameters.getSchema().getValue()))) {
            schemaErrors.add(new ServiceError("schema", "No schema found"));
        }

        return schemaErrors;
    }

    private static Set<ServiceError> validateObjectType(final PayrollReportParameters reportParameters) {
        final Set<ServiceError> objectTypeErrors = Sets.newHashSet();

        if (Objects.isNull(reportParameters.getObjectType())) {
            objectTypeErrors.add(new ServiceError("objectType", "No object type found"));
        }

        return objectTypeErrors;
    }

    private static Set<ServiceError> validateObjectTypeAttributes(final List<ObjectTypeAttributeBean> attributes) {
        final Set<ServiceError> attributeErrors = Sets.newHashSet();

        if (attributes.isEmpty()) {
            attributeErrors.add(new ServiceError("objectType", "Could not find any associated attributes"));
            return attributeErrors;
        }

        return attributeErrors;
    }

    private static Set<ServiceError> validateTypes(final String paramName,
                                            final Integer attrTypeId,
                                            final Map<Integer, ObjectTypeAttributeBean> objectTypeAttributeBeanMap,
                                            final Set<ObjectTypeAttributeBean.DefaultType> expectedTypes) {

        final Set<ServiceError> typeExpectationErrors = Sets.newHashSet();

        if (!objectTypeAttributeBeanMap.containsKey(attrTypeId)) {
            typeExpectationErrors.add(new ServiceError(paramName, "Attribute does not exist"));
        } else if (!expectedTypes.contains(objectTypeAttributeBeanMap.get(attrTypeId).getDefaultType())) {
            typeExpectationErrors.add(new ServiceError(paramName, "Attribute not a valid numeric type"));
        }

        return typeExpectationErrors;
    }

    private static Set<ServiceError> validateDates(Period period, final LocalDateTime startDate, final LocalDateTime endDate) {
        final Set<ServiceError> dateValidation = Sets.newHashSet();

        if (Period.CUSTOM == period && (Objects.isNull(startDate) || Objects.isNull(endDate))) {
            dateValidation.add(new ServiceError("period", "Custom Period must specify start and end dates"));
        }

        if ((Objects.nonNull(startDate) || Objects.nonNull(endDate)) && startDate.isAfter(endDate)) {
            dateValidation.add(new ServiceError("startDate", "Start Date cannot be after End Date"));
        }

        return dateValidation;
    }

}
