var BarChart = {};

BarChart.Mapper = function (data, parameters, baseUrl) {

    var mapper = new PayrollMapper(data, parameters);

    var expenditureIn = {
        label: "IN",
        data: [],
        backgroundColor: 'rgba(255,57,57,0.5)',
        borderColor: 'rgb(255,57,57)',
        borderWidth: 1
    };

    var expenditureOut = {
        label: "OUT",
        data: [],
        backgroundColor: 'rgba(45,218,181,0.5)',
        borderColor: 'rgb(45,218,181)',
        borderWidth: 1
    };

    var expenditureTotal = {
        label: "TOTAL",
        data: [],
        backgroundColor: 'rgba(111,158,255,0.5)',
        borderColor: 'rgb(111,158,255)',
        borderWidth: 1
    };

    return mapper.asTimeSeries(expenditureIn, expenditureOut, expenditureTotal);

};

BarChart.View = function (mappedData) {

    var containingElement = document.querySelector('.js-riada-widget');
    if (!containingElement) return;

    var clonedData = JSON.parse(JSON.stringify(mappedData));

    var canvas = new Canvas("myChart");

    var canvasElement = canvas.appendTo(containingElement);

    var myChart = new Chart(canvasElement, {
        type: 'bar',
        data: clonedData,
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    stacked: true
                }]
            },
            animation: {
                duration: 0
            },
            responsive: true,
            maintainAspectRatio: false
        }
    });

    return containingElement;
};

var AreaChart = {};

AreaChart.Mapper = function (data, parameters, baseUrl) {

    var mapper = new PayrollMapper(data, parameters);

    var expenditureIn = {
        label: "IN",
        data: [],
        backgroundColor: 'rgba(255,57,57,0.5)',
        steppedLine: true,
        pointRadius: 2,
    };

    var expenditureOut = {
        label: "OUT",
        data: [],
        backgroundColor: 'rgba(45,218,181,0.5)',
        steppedLine: true,
        pointRadius: 2
    };

    var expenditureTotal = {
        label: "TOTAL",
        data: [],
        backgroundColor: 'rgba(111,158,255, 0.5)',
        steppedLine: true,
        pointRadius: 2
    };

    return mapper.asTimeSeries(expenditureIn, expenditureOut, expenditureTotal);

};

AreaChart.View = function (mappedData) {

    var containingElement = document.querySelector('.js-riada-widget');
    if (!containingElement) return;

    var clonedData = JSON.parse(JSON.stringify(mappedData));

    var canvas = new Canvas("myChart");

    var canvasElement = canvas.appendTo(containingElement);

    var myChart = new Chart(canvasElement, {
        type: 'line',
        data: clonedData,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            elements: {
                line: {
                    tension: 0
                }
            },
            animation: {
                duration: 0
            },
            responsive: true,
            maintainAspectRatio: false
        }
    });

    return containingElement;
};

var Canvas = function (id) {
    this.id = id;

    this.appendTo = function (containingElement) {

        clearOldIfExists(containingElement);

        var canvasElement = document.createElement("canvas");
        canvasElement.id = this.id;

        containingElement.appendChild(canvasElement);

        return canvasElement;
    };

    function clearOldIfExists(containingElement) {
        var oldCanvas = containingElement.querySelector('#myChart');
        if (oldCanvas) oldCanvas.remove();
    }
};

var PayrollMapper = function (data, parameters) {
    this.data = data;
    this.parameters = parameters;

    var EXPENDITURE_IN = "IN";
    var EXPENDITURE_OUT = "OUT";
    var EXPENDITURE_TOTAL = "TOTAL";

    this.asTimeSeries = function (dataIn, dataOut, dataTotal) {
        var mappedData = {};

        if (!this.data.metadata.hasData || this.parameters.numAttribute == null) {
            return mappedData;
        }

        mappedData.labels = Object.keys(data.expendituresByDay);
        mappedData.datasets = [];

        var attributeMap = createAttributeMap(this.parameters, dataIn, dataOut, dataTotal);

        Object.entries(data.expendituresByDay).forEach(function (entry, index) {

            var expenditures = entry[1];

            if (expenditures === undefined || expenditures.length === 0) {

                Object.entries(attributeMap).forEach(function (entry) {
                    var expenditure = entry[1];

                    fillData(expenditure, EXPENDITURE_IN, 0.0);
                    fillData(expenditure, EXPENDITURE_OUT, 0.0);

                    var previousTotal = index === 0 ? 0.0 : expenditure[EXPENDITURE_TOTAL].data[index - 1];
                    fillData(expenditure, EXPENDITURE_TOTAL, previousTotal);
                });

            }

            expenditures.forEach(function (expenditure) {
                if (attributeMap.hasOwnProperty(expenditure.name)) {
                    fillData(attributeMap[expenditure.name], EXPENDITURE_IN, expenditure.typeValueMap[EXPENDITURE_IN]);
                    fillData(attributeMap[expenditure.name], EXPENDITURE_OUT, 0.0 - expenditure.typeValueMap[EXPENDITURE_OUT]);

                    var currentTotal = expenditure.typeValueMap[EXPENDITURE_IN] - expenditure.typeValueMap[EXPENDITURE_OUT];
                    var previousTotal = index === 0 ? 0.0 : attributeMap[expenditure.name][EXPENDITURE_TOTAL].data[index - 1];
                    fillData(attributeMap[expenditure.name], EXPENDITURE_TOTAL, currentTotal + previousTotal)
                }
            });

        });

        mappedData.datasets = flatMap(attributeMap);

        return mappedData;

    };

    var createAttributeMap = function (parameters, dataIn, dataOut, dataTotal) {
        var map = {};

        map[parameters.numAttribute.label] = {};

        dataIn.label = parameters.numAttribute.label + "-" + dataIn.label;
        dataOut.label = parameters.numAttribute.label + "-" + dataOut.label;
        dataTotal.label = parameters.numAttribute.label + "-" + dataTotal.label;

        map[parameters.numAttribute.label][EXPENDITURE_IN] = dataIn;
        map[parameters.numAttribute.label][EXPENDITURE_OUT] = dataOut;
        map[parameters.numAttribute.label][EXPENDITURE_TOTAL] = dataTotal;

        return map;
    };

    function fillData(expenditure, dataType, value) {
        expenditure[dataType].data.push(value);
    }

    function flatMap(attributeMap) {
        var flattened = [];
        Object.values(attributeMap).forEach(function (valuesByAttribute) {
            Object.values(valuesByAttribute).forEach(function (value) {
                flattened.push(value);
            });
        });

        return flattened;
    }

};

var Transformer = {};

Transformer.JSON = function (mappedData) {
    if(!mappedData) return null;

    mappedData.datasets.forEach(function(dataset){
       ignoringKeys(['_meta'], dataset);
    });

    return JSON.stringify(mappedData);
};

function ignoringKeys(keys, data){
    keys.forEach(function(key){
        delete data[key];
    })
}