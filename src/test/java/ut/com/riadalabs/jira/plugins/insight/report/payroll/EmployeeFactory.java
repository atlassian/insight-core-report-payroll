package ut.com.riadalabs.jira.plugins.insight.report.payroll;

import com.google.common.collect.Lists;
import com.riadalabs.jira.plugins.insight.services.model.MutableObjectAttributeBean;
import com.riadalabs.jira.plugins.insight.services.model.MutableObjectAttributeValueBean;
import com.riadalabs.jira.plugins.insight.services.model.MutableObjectBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectAttributeBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectAttributeValueBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectBean;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class EmployeeFactory {


    public static ObjectBean createEmployeeBean(final Integer id, final Integer objectTypeId, Map<Integer, Double> numberAttributeWithValuesMap,
                                                final Map<Integer, LocalDate> dateAttributeWithValuesMap) {

        final MutableObjectBean employee = new MutableObjectBean();
        employee.setId(id);
        employee.setObjectKey("Employee-" + id);

        employee.setObjectTypeId(objectTypeId);

        final List<ObjectAttributeBean> numericAttributeBeans = numberAttributeWithValuesMap.entrySet()
                .stream()
                .map(entry -> createNumberTypeObjectAttributeBean(id, entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        final List<ObjectAttributeBean> dateAttributeBeans = dateAttributeWithValuesMap.entrySet()
                .stream()
                .map(entry -> createDateTypeObjectAttributeBean(id, entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        numericAttributeBeans.addAll(dateAttributeBeans);

        employee.setObjectAttributeBeans(numericAttributeBeans);

        return employee;

    }

    private static ObjectAttributeBean createNumberTypeObjectAttributeBean(final Integer employeeId,
                                                                           final Integer objectTypeAttributeId, final Double value) {

        final MutableObjectAttributeBean objectAttributeBean = new MutableObjectAttributeBean();
        objectAttributeBean.setId(objectTypeAttributeId);
        objectAttributeBean.setObjectTypeAttributeId(objectTypeAttributeId);
        objectAttributeBean.setObjectId(employeeId);

        final List<ObjectAttributeValueBean> objectAttributeValueBeans = Lists.newArrayList();

        final MutableObjectAttributeValueBean numberAttributeValueBean = new MutableObjectAttributeValueBean();
        numberAttributeValueBean.setId(objectTypeAttributeId);
        numberAttributeValueBean.setDoubleValue(value);

        objectAttributeValueBeans.add(numberAttributeValueBean);

        objectAttributeBean.setObjectAttributeValueBeans(objectAttributeValueBeans);

        return objectAttributeBean;
    }

    private static ObjectAttributeBean createDateTypeObjectAttributeBean(final Integer employeeId,
                                                                         final Integer objectTypeAttributeId, final LocalDate date) {

        final MutableObjectAttributeBean objectAttributeBean = new MutableObjectAttributeBean();
        objectAttributeBean.setId(objectTypeAttributeId);
        objectAttributeBean.setObjectTypeAttributeId(objectTypeAttributeId);
        objectAttributeBean.setObjectId(employeeId);

        final List<ObjectAttributeValueBean> objectAttributeValueBeans = Lists.newArrayList();

        final MutableObjectAttributeValueBean dateAttributeValueBean = new MutableObjectAttributeValueBean();
        dateAttributeValueBean.setId(objectTypeAttributeId);

        dateAttributeValueBean.setDateValue(Objects.isNull(date) ? null : toDate(date));

        objectAttributeValueBeans.add(dateAttributeValueBean);

        objectAttributeBean.setObjectAttributeValueBeans(objectAttributeValueBeans);

        return objectAttributeBean;
    }

    private static Date toDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay()
                .atZone(ZoneId.of("UTC"))
                .toInstant());
    }

}
