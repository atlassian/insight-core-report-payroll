package ut.com.riadalabs.jira.plugins.insight.report.payroll;

import com.riadalabs.jira.plugins.insight.reports.payroll.PayrollReportParameters;
import com.riadalabs.jira.plugins.insight.reports.payroll.Period;
import com.riadalabs.jira.plugins.insight.reports.payroll.Value;
import com.riadalabs.jira.plugins.insight.services.model.MutableObjectTypeAttributeBean;
import com.riadalabs.jira.plugins.insight.services.model.MutableObjectTypeBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeAttributeBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeBean;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class PayrollHelperFactory {

    public static PayrollReportParameters createBasicReportWithinLastMonth(final Integer schemaId, final Integer objectTypeId,
            final Integer numberTypeAttribute, final Integer startOfExpense, final Integer endOfExpense) {

        final PayrollReportParameters parameters = new PayrollReportParameters();

        parameters.setSchema(new Value(schemaId));
        parameters.setObjectType(new Value(objectTypeId));
        parameters.setNumAttribute(new Value(numberTypeAttribute));
        parameters.setPeriod(Period.LAST_MONTH.name());
        parameters.setDateAttributeStartDate(new Value(startOfExpense));
        parameters.setDateAttributeEndDate(new Value(endOfExpense));

        return parameters;
    }

    public static PayrollReportParameters createBasicReportWithinCustomPeriod(final Integer schemaId, final Integer objectTypeId,
            final Integer numberTypeAttribute, final Integer startOfExpense, final Integer endOfExpense, final LocalDateTime startDate,
            final LocalDateTime endDate) {

        final PayrollReportParameters parameters = new PayrollReportParameters();

        parameters.setSchema(new Value(schemaId));
        parameters.setObjectType(new Value(objectTypeId));
        parameters.setNumAttribute(new Value(numberTypeAttribute));
        parameters.setPeriod(Period.CUSTOM.name());
        parameters.setDateAttributeStartDate(new Value(startOfExpense));
        parameters.setDateAttributeEndDate(new Value(endOfExpense));
        parameters.setStartDate(startDate);
        parameters.setEndDate(endDate);

        return parameters;
    }

    public static ObjectTypeBean createObjectType(final Integer schemaId, final Integer typeId) {
        MutableObjectTypeBean employeeType = new MutableObjectTypeBean(typeId);
        employeeType.setObjectSchemaId(schemaId);
        employeeType.setName("Employee");

        return employeeType;
    }

    public static ObjectTypeAttributeBean createObjectTypeAttributeBean(final Integer id,
            final String name,
            final ObjectTypeAttributeBean.DefaultType type) {

        final MutableObjectTypeAttributeBean mutableObjectTypeAttributeBean = new MutableObjectTypeAttributeBean();
        mutableObjectTypeAttributeBean.setId(id);
        mutableObjectTypeAttributeBean.setName(name);
        mutableObjectTypeAttributeBean.setDefaultType(
                Objects.isNull(type) ? ObjectTypeAttributeBean.DefaultType.NONE : type);

        return mutableObjectTypeAttributeBean;
    }

}
