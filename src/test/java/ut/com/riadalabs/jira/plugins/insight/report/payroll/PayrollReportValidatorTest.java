package ut.com.riadalabs.jira.plugins.insight.report.payroll;

import com.google.common.collect.Lists;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectSchemaFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeAttributeFacade;
import com.riadalabs.jira.plugins.insight.common.exception.InsightException;
import com.riadalabs.jira.plugins.insight.reports.payroll.PayrollReportParameters;
import com.riadalabs.jira.plugins.insight.reports.payroll.Period;
import com.riadalabs.jira.plugins.insight.reports.payroll.validator.PayrollReportValidator;
import com.riadalabs.jira.plugins.insight.services.model.ObjectSchemaBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeAttributeBean;
import io.riada.core.service.model.ServiceError;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PayrollReportValidatorTest {

    private final static Integer schemaId = 10;
    private final static ObjectSchemaBean validSchema = new ObjectSchemaBean(schemaId);

    private final static Integer objectTypeId = 20;
    private final static Integer canadianSalaryId = 30;

    private final static String canadianSalaryName = "SALARY(CAD)";

    private final static Integer startDateId = 50;
    private final static Integer endDateId = 60;

    private final static String startDateName = "START_DATE";
    private final static String endDateName = "END_DATE";

    private final ObjectSchemaFacade objectSchemaFacadeMock = mock(ObjectSchemaFacade.class);
    private final ObjectTypeAttributeFacade objectTypeAttributeFacadeMock = mock(ObjectTypeAttributeFacade.class);

    private PayrollReportParameters payrollReportParameters;

    @Before
    public void setUp() {
        this.payrollReportParameters =
                PayrollHelperFactory.createBasicReportWithinLastMonth(schemaId, objectTypeId, canadianSalaryId,
                        startDateId, endDateId);
    }

    @Test
    public void should_be_invalid_no_schema() throws InsightException {
        final ServiceError expectedError = new ServiceError("schema", "No schema found");

        when(objectSchemaFacadeMock.loadObjectSchemaBean(schemaId))
                .thenReturn(null);

        final Set<ServiceError> errors = PayrollReportValidator.validate(this.payrollReportParameters, objectSchemaFacadeMock,
                objectTypeAttributeFacadeMock);

        assertThat(errors).hasSize(1)
                .as("No Schema Found Error")
                .contains(expectedError);
    }


    @Test
    public void should_be_invalid_empty_attributes() throws InsightException {
        final ServiceError expectedError = new ServiceError("objectType", "Could not find any associated attributes");

        when(objectSchemaFacadeMock.loadObjectSchemaBean(schemaId))
                .thenReturn(validSchema);

        when(objectTypeAttributeFacadeMock.findObjectTypeAttributeBeans(objectTypeId))
                .thenReturn(Lists.newArrayList());

        final Set<ServiceError> errors = PayrollReportValidator.validate(this.payrollReportParameters, objectSchemaFacadeMock,
                objectTypeAttributeFacadeMock);

        assertThat(errors).hasSize(1)
                .as("Empty Attributes Error")
                .contains(expectedError);
    }

    @Test
    public void should_be_invalid_if_start_date_after_end_date() throws InsightException {
        final ServiceError expectedError = new ServiceError("startDate", "Start Date cannot be after End Date");

        this.payrollReportParameters.setPeriod(Period.CUSTOM.name());

        this.payrollReportParameters.setStartDate(LocalDateTime.now());
        this.payrollReportParameters.setEndDate(LocalDateTime.now()
                .minusMonths(1));

        final ObjectTypeAttributeBean numberType =
                PayrollHelperFactory.createObjectTypeAttributeBean(canadianSalaryId, canadianSalaryName,
                        ObjectTypeAttributeBean.DefaultType.DOUBLE);

        final ObjectTypeAttributeBean startDateType =
                PayrollHelperFactory.createObjectTypeAttributeBean(startDateId, startDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE);

        final ObjectTypeAttributeBean endDateType =
                PayrollHelperFactory.createObjectTypeAttributeBean(endDateId, endDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE);

        when(objectSchemaFacadeMock.loadObjectSchemaBean(schemaId))
                .thenReturn(validSchema);

        when(objectTypeAttributeFacadeMock.findObjectTypeAttributeBeans(objectTypeId))
                .thenReturn(Lists.newArrayList(numberType, startDateType, endDateType));

        final Set<ServiceError> errors = PayrollReportValidator.validate(this.payrollReportParameters, objectSchemaFacadeMock,
                objectTypeAttributeFacadeMock);

        assertThat(errors).hasSize(1)
                .as("Has Invalid Dates")
                .contains(expectedError);
    }

    @Test
    public void should_be_invalid_if_given_numeric_attribute_not_a_numeric_type() throws InsightException {

        final ServiceError expectedError = new ServiceError("numAttribute", "Attribute not a valid numeric type");

        final ObjectTypeAttributeBean invalidType =
                PayrollHelperFactory.createObjectTypeAttributeBean(canadianSalaryId, canadianSalaryName,
                        ObjectTypeAttributeBean.DefaultType.EMAIL);

        final ObjectTypeAttributeBean startDateType =
                PayrollHelperFactory.createObjectTypeAttributeBean(startDateId, startDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE);

        final ObjectTypeAttributeBean endDateType =
                PayrollHelperFactory.createObjectTypeAttributeBean(endDateId, endDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE);

        when(objectSchemaFacadeMock.loadObjectSchemaBean(schemaId))
                .thenReturn(validSchema);

        when(objectTypeAttributeFacadeMock.findObjectTypeAttributeBeans(objectTypeId))
                .thenReturn(Lists.newArrayList(invalidType, startDateType, endDateType));

        final Set<ServiceError> errors = PayrollReportValidator.validate(this.payrollReportParameters, objectSchemaFacadeMock,
                objectTypeAttributeFacadeMock);

        assertThat(errors).hasSize(1)
                .as("EmailType Is Invalid For Numeric Attribute")
                .contains(expectedError);
    }

    @Test
    public void should_be_valid() throws InsightException {
        final ObjectTypeAttributeBean numberType =
                PayrollHelperFactory.createObjectTypeAttributeBean(canadianSalaryId, canadianSalaryName,
                        ObjectTypeAttributeBean.DefaultType.DOUBLE);

        final ObjectTypeAttributeBean startDateType =
                PayrollHelperFactory.createObjectTypeAttributeBean(startDateId, startDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE);

        final ObjectTypeAttributeBean endDateType =
                PayrollHelperFactory.createObjectTypeAttributeBean(endDateId, endDateName,
                        ObjectTypeAttributeBean.DefaultType.DATE);

        when(objectSchemaFacadeMock.loadObjectSchemaBean(schemaId))
                .thenReturn(validSchema);

        when(objectTypeAttributeFacadeMock.findObjectTypeAttributeBeans(objectTypeId))
                .thenReturn(Lists.newArrayList(numberType, startDateType, endDateType));

        final Set<ServiceError> errors = PayrollReportValidator.validate(this.payrollReportParameters, objectSchemaFacadeMock,
                objectTypeAttributeFacadeMock);

        assertThat(errors).as("Valid")
                .isEmpty();
    }
}
