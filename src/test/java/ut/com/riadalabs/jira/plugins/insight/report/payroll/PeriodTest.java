package ut.com.riadalabs.jira.plugins.insight.report.payroll;

import com.riadalabs.jira.plugins.insight.reports.payroll.Period;
import org.junit.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class PeriodTest {

    private static final LocalDate currentDate = LocalDate.of(2019, 1, 1); // Tuesday
    private static final boolean doesWeekBeginOnMonday = false;

    @Test
    public void should_return_default_period_if_null_or_unknown() {
        final String nulled = null;
        final String bogus = "N/A";

        assertThat(Period.defaultPeriod()).isEqualByComparingTo(Period.from(nulled));
        assertThat(Period.defaultPeriod()).isEqualByComparingTo(Period.from(bogus));
    }

    @Test
    public void should_return_current_week() {
        final LocalDate expectedBeginning = LocalDate.of(2018, 12, 30);

        assertThat(Period.CURRENT_WEEK.from(currentDate, doesWeekBeginOnMonday)).as("Beginning of current week")
                .isEqualTo(expectedBeginning);

        assertThat(Period.CURRENT_WEEK.to(currentDate, doesWeekBeginOnMonday)).as("End of week does not exceed today")
                .isEqualTo(currentDate);
    }

    @Test
    public void should_return_current_month() {
        final LocalDate expectedBeginning = LocalDate.of(2019, 1, 1);

        assertThat(Period.CURRENT_MONTH.from(currentDate, doesWeekBeginOnMonday)).as("Beginning of current month")
                .isEqualTo(expectedBeginning);

        assertThat(Period.CURRENT_MONTH.to(currentDate, doesWeekBeginOnMonday)).as("End of current month does not exceed today")
                .isEqualTo(currentDate);
    }

    @Test
    public void should_return_current_year() {
        final LocalDate expectedBeginning = LocalDate.of(2019, 1, 1);

        assertThat(Period.CURRENT_YEAR.from(currentDate, doesWeekBeginOnMonday)).as("Beginning of current year")
                .isEqualTo(expectedBeginning);

        assertThat(Period.CURRENT_YEAR.to(currentDate, doesWeekBeginOnMonday)).as("End of current year does not exceed today")
                .isEqualTo(currentDate);
    }

    @Test
    public void should_return_last_week() {
        final LocalDate expectedBeginning = LocalDate.of(2018, 12, 23);
        final LocalDate expectedEnd = LocalDate.of(2018, 12, 29);

        assertThat(Period.LAST_WEEK.from(currentDate, doesWeekBeginOnMonday)).as("Beginning of last week")
                .isEqualTo(expectedBeginning);

        assertThat(Period.LAST_WEEK.to(currentDate, doesWeekBeginOnMonday)).as("End of last week")
                .isEqualTo(expectedEnd);
    }

    @Test
    public void should_return_last_month() {
        final LocalDate expectedBeginning = LocalDate.of(2018, 12, 1);
        final LocalDate expectedEnd = LocalDate.of(2018, 12, 31);

        assertThat(Period.LAST_MONTH.from(currentDate, doesWeekBeginOnMonday)).as("Beginning of last month")
                .isEqualTo(expectedBeginning);

        assertThat(Period.LAST_MONTH.to(currentDate, doesWeekBeginOnMonday)).as("End of last month")
                .isEqualTo(expectedEnd);
    }

    @Test
    public void should_return_last_year() {
        final LocalDate expectedBeginning = LocalDate.of(2018, 1, 1);
        final LocalDate expectedEnd = LocalDate.of(2018, 12, 31);

        assertThat(Period.LAST_YEAR.from(currentDate, doesWeekBeginOnMonday)).as("Beginning of last year")
                .isEqualTo(expectedBeginning);

        assertThat(Period.LAST_YEAR.to(currentDate, doesWeekBeginOnMonday)).as("End of last year")
                .isEqualTo(expectedEnd);
    }

    @Test
    public void should_return_custom_range() {
        final LocalDate customStart = currentDate.minusYears(2)
                .minusMonths(2)
                .minusWeeks(2);

        final LocalDate customEnd = currentDate.plusYears(5);

        assertThat(Period.CUSTOM.from(customStart, doesWeekBeginOnMonday)).as("Custom period start")
                .isEqualTo(customStart);

        assertThat(Period.CUSTOM.to(customEnd, doesWeekBeginOnMonday)).as("Custom period end")
                .isEqualTo(customEnd);
    }
}
